Yes, Facebook: privacy does matter. Here's why
==============================================

Can we be ourselves online?
---------------------------

Several years ago, I was working with a Lebanese client in the Middle 
East. We had a good relationship - we shared a love of Australian 
beaches and London pubs. He spoke about his sadness at leaving Beirut. 
I spoke about missing the friends I grew up with in England.

One day, I got a call from my E.A. - did I realise that all my
photos on Facebook were now visible to everyone?
Facebook had changed my privacy settings without my consent, 
or even informing me [^1]. 
There were photos of my kids births I had 
shared with my sister, photos of my home in Bondi and photos of my 
parents house. Most immediately concerning was that a visitor to my
page might reasonably infer that I was probably Jewish.

In fact, while my parents *are* practising Jews, I do not consider
myself Jewish. But clearly it is not something I wanted my client to
know about me. Fortunately I was able to remove all of my photos
before any damage was done.

The fact is, I am a somewhat different person with different groups of 
people. A feat made easier because the various groups are widely
separated geographically and contextually with few overlaps.

At work, I present a professional face and I would prefer it if those
I work with didn't know too much about my the details of my private 
life. My friends from university days know one version of me - more
irreverent, anti-authoritarian, sporty and risk-taking. My friends 
from high school remember me as shy, brooding and serious child. And
the people I met when I was a triathlon enthusiast know a more blokey
and down to earth version of me. I even have different nicknames in
each group.

Even with my closest friends I feel a strong instinct to prevent some
of the 'crazy' coming out: I don't share the full extent of the private
paranoia, self-obsessions and self-doubt that can overcome me in darker
moments, lest they wonder why they choose to spend time with me.

Perhaps I am an anachronism or an exception. But even if my identity is
more fragmented than most, we all have some sense of 'multiple selves'.
Millenials and future generations may find they do not have the ability
to manage multiple identifies in an era of Facebook and Google, because
on the internet it is far harder to maintain boundaries between our
various 'circles' (to use Google's term).

Facebook's total indifference to this need to control who sees what
violates our control of our various public selves and undermines an
important part of how we interact in the world.

Can we ever escape our past?
----------------------------

I read with dismay the recent news that 'Guardians of the Galaxy' 
director, James Gunn had been fired by Disney for a series of Twitter
comments he had made 10-years ago. They had been an attempt to gain
attention as a 'shock comic' before he had become a successful
director. The tweets had been unearthed by an ultra-right-wing 
conspiracy theorist called Mike Cernovich, apparently in retaliation
for some anti-Trump comments Gunn had made. [^2]

Gunn's response is revealing:

> "A couple of years ago I wrote a blog that was meant to be satirical
> and funny. In rereading it over the past day I don't think it's funny.
> The attempted humor in the blog does not represent my actual feelings.
> People who are familiar with me as evidenced by my Facebook page
> and other mediums know that I'm an outspoken proponent for the rights
> of the gay and lesbian community, women and anyone who feels
> disenfranchised ... I'm learning all the time."

The worrying thought is that we all have past selves we are
embarrassed by: we cannot always defend the values we once held, often
a carry-over from our families, schools and role models when we were
still maturing. And as we learn more, think more, see more, we continue
to change out views. We are all constantly evolving.

I would feel mortified if I were judged today by the views I held when
I first started university, with assumptions about nationalism and
homosexuality for example I absorbed from years at a British Public
school [^3]. I participated in conversations with other students that
make me cringe today - it was a process of personal discovery - thinking
deeply to find out for myself what I really thought. Free and open
discussion, and exploring a range of different perspectives,
eventually helped me solidify a strong liberal conviction that only
emerged fully later in life.

A key part of the journey we take during our teenage years is to 
'find ourselves': to try on different identities and values, 
to experiment with ways of being in the world, and ultimately to
take responsibility for a coherent set of values that will define
our character as adults. To live as a teenager in a world where you
can never shed your past comments, choices or behaviours is to be
bound to the unquestioned values of your youthful self, the place you
grew up in and the people you grew up with, ossified and memorialised
by your social media history, and watch it diverge further and further
from the person you are becoming.

To be judged by our past is to preclude the idea of personal growth.
It prevents us from providing temporal or situational explanatory context.
It blurs past evidence with current actuality.

Our immature selves are strangers to us, but online we will be eternally
held accountable for that stranger's misdemeanours.

As your behaviour suspicious?
-----------------------------

In one short period, I bought the following books:

* Marx - The Communist Party Manifesto
* Koblitz - A Course in Number Theory and Cryptography
* Schneier - Applied Cryptography
* Levy - Hackers: Heroes of the Computer Revolution [^4]

At roughly the same time, I made the following searches on the internet:

* Anonymous
* TOR Onion Router
* 4chan cryptography [^5]

If that combination didn't trigger a big red flag somewhere in 
a government security agency computer somewhere I would be shocked. 
And if it didn't at the time, I feel certain that it would today.

But please let me explain myself.

I had to give a presentation at work in support of a cultural
change initiative. Staring at a blank sheet of paper, I sought out
some inspiration: who else has faced the task of inspiring a
a dejected group of people to make a difficult change? 
Martin Luthur King? Pericles? 
Well, I don't sympathise much with Marx's ideology, 
but there is no denying his rhetorical skill.

I was also reading Neal Stephenson's Cryptonomicon at the time at the
recommendation of a friend [^6]. And when I get really into a book,
I love to read around it too: I hit Wikipedia hard - I want to know
the history, the background, the references, the allusions, the
details. I quickly came upon security guru Bruce Schneier, and Koblitz'
excellent introductory text. 
My web searched also led me to a difficult cryptography problem that 
had been posted on 4chan [^7] that was thought to be a recruiting 
exercise for the cracker-activist collective, Anonymous. 
I didn't get far with it.

Levy's 'Hackers' has nothing to do with the kind of hacking that
Anonymous are known for. The book is about a group of 1970's MIT 
uber-coders and artificial intelligence pioneers and their followers 
who experimented playfully with MIT's PDP-10 computers in the 1970s. 

The point is that mass surveillance of the kind that Edward Snowden
brought to light with his revelations about PRISM [^8], mean that we can
find ourselves suspected of activities based on our online behaviours without
any chance to defend or explain ourselves.

And because the trail we leave on the internet is necessarily a partial
and haphazard record of our overall activities and interests, 
it is particularly prone to over-interpretation and mis-representation.

Worse still is that such 'suspicions' based on our online records can
be picked up by anyone, not just government surveillance programs. 
Indeed, it is almost as easy for a malicious private citizen to get 
hold of information about us (usually even more partial and biased). 
Recent Freedom of Information requests to Facebook, show just how much
a private company can collect [^9]. This includes surprisingly comprehensive
information gathered in addition to information contained in Facebook
user posts and includes a great deal of data on people who have never 
even held a Facebook accout [^10].

What do you have to hide?
-------------------------

The most common arguments I've heard in defence of this kind of data
gathering and surveillance are along the lines of - What does it matter
if you have nothing to hide? 

Leaving aside whistleblowers, persecuted minorities and truth-speakers
in authoritarian regimes, even those with impeccible records should
fear where this line of thinking leads.

I once wrote a comment on a Hacker News that criticised Nassim
Nicholas Taleb's rather thuggish Twitter attack on a liberal academic
whose views he disagreed with [^11]. The attack was magnified by the
aggression of an army of Taleb's Twitter acolytes who followed up with
deeply mysogynistic abuse. Taleb's 'Twitter army' is well known as a
cynically abusive and destructive force of 'trolls' that frequently 
attempts to put pressure on employers to sack Taleb's critics, or otherwise
embarrass them. For a brief period, I too came into their purview
and I found all of my Hacker News contributions had been heavily 
downvoted and attacked apparently in retaliation. Fortunately, they
quickly tired of me as target and apparently failed to find anything
really embarrassing they could connect to me before moving on to
easier targets.

More innocuously, we might find that we are judged unsuitable for a job
because they unearthed a photo from our college days taken on a night out
in which we are drinking and smoking that suggests we might not be the 
'kind of person we are looking for'.

More startlingly, some friends of ours whose daughter is an excellent
chess player routinely withdrew her from tournaments in which she had 
been paired with a particular player at her club. It transpired that they
had searched his name online and uncovered an article from another country
that suggested he had once been a suspect in a murder case. I have to
admit a sympathy for their protective instincts and I would hate to put
my children at risk. However, one of the great vitues of chess is that
it is a controlled environment where children and adults, men and women,
successful and destitute, healthy and disabled, virtuous and dispicable
can shed all but their love of the game and match wits on equal terms.
Chess has its fair share of 'characters', but with no other information
available except for one unsubstantiated accusation on one player, 
the parents had made use of what they had to make their decision. It is
not the decision that worries me, but the way a small piece of evidence
can be magnified when nothing else is available.

We shed random pieces of data behind us as we meander around on the 
internet: arbitrary contextless snippets of information to be 
over-interpreted and recycled as ammunition for trolls and 'social
activists' who would judge and convict us on them with no opportunity
to explain or defend ourselves. Once accused we enter a Kafka's castle
of illogic and implaccability where we can never face our accuser or
fully understand the nature of our supposed crime.

James Gunn isn't the first victim of such mass vigilantism and will not
be the last. Companies often have no idea how to handle explosions of
rage that well up against their employess and their knee-jerk reactions
exaccerbate a bad situation. The victims, meanwhile, have no recourse.

Are online privacy breaches the same as trespassing?
----------------------------------------------------

Trespassing feels like a violation. We don't like the
idea that someone is looking through our belongings, fingering our
precious belongings, and riffling through our drawers. I'm not quite
certain why this feels so uncomfortable. Is it that we fear theft,
or that we fear that our private space will somehow be 'polluted'?
For whatever reason, it is a visceral feeling and
one that is usually protected by law in the tanglible world.

To some extent, I think we have a similar sense of 'private space'
online: a cloud drive for example, or private messages. We expect that
the providers of these services, be it Dropbox or WhatsApp, will
understand and respect this. When Facebook changed my privacy settings
unilaterally, it felt like a betrayal and would have done so whether or
not there was actually anything exposed. I felt it in the same way as
if they had removed the locks on the front door of my house so that
any stranger could walk in and creep around, whether or not anyone
actually had done so.

To some extent it shows that my expectations were naive. Facebook had
no intention of providing me with a 'private space' and I was mistaken
to expect it. But increasingly, we do need to find a way to create private
spaces to share things on the internet with friends and family. 
Who *can* we trust to provide them?

What obligations does Social Media have?
----------------------------------------

In dealing with companies like Facebook, what exactly are we agreeing
to? What are the terms of the implicit contract we have with them [^12]?

Some companies - notably Apple and Signal - seem to offer us some
kind of privacy for our information.
But as Facebook showed with their willingness to alter privacy
settings of their customers, this cannot be taken for granted.
WhatsApp seemed keen to protect conversations between people from
prying eyes, but their acquisition by Facebook and the likelihood that
they are sharing data with the US Government suggest that this is
probably not the case.

We are happy to share some information with social media companies:
we tell Amazon what we thought of our purchases, so it can better 
recommend new products to us. This seems like a fair trade. To some
extent we would be willing to share more information, to help Amazon
inform us of new products we might find relevant. So long as we
understand what we are trading for what, it seems like an acceptable
deal.

In a similar way, we share our social connections and interests with
Facebook in the expectation that they will use that information to
help us to manage our connections: to identify people likely to be in
our social circles; to identify information shared by our friends we
are likely to be interested in; and to help people we meet to locate 
and connect with us. 
We expect that the data we provide will be used in good faith, on
our terms. But this is not how Facebook behaves. 
They collect vastly more information on us than we share explicitly to
generate a 'saleable user profile' on us to sell to advertisers and
data anayltics companies. If this helps to show us advertising that
is more relevant to us, that seems like a service, particularly when
the rest of the platform is free. But it can verge on the creepy:
How does Facebook know I've been researching running shoes?
Can they read my web searches? 
Is every site with a Facebook 'like' button on it sending them data?
If so, how are they connecting it to me?
Can they get information from sites that do not explicitly contain
Facebook links?
Are they able to see other sites I have open on my browser?
Or get access to my email contacts?

This information is sold to marketing and research firms whose purposes
are definitely not in my interests, as shown by Cambridge Analytica's
use of Facebook-obtained data to support Donald Trump's presidential
bid [^10].

But they also use their information for other more directly sinister 
purposes. 
For example promoting click-bait 'news' posts that are indistinguishable
from news posts from reputable sites, but which lead to thin, nonsensical
content, crowded out by advertising, malware downloads and yet more fake,
click-bait headlines.
When they do provide 'news', it is more often selected to generate
'engagement' (read 'outrage') than information, and is often
known to be false.
Revealingly, when I post fact-checking refutations of stories,
they are disseminated far less widely amongst my contacts than the
original false story.

Facebook is also very selective in which posts I get to see
from my friends - it was happy to tell me that an acquaintance had
shared a (false) story about a celebrity, but not that my closest
friend from college had got engaged. Clearly, this is not a service
focused on my best interests. It shows a worrying level of power,
being increasingly divergent from the promise of helping me
manage my social connections.

The caveat that "if you aren't the customer, you're the product"
clearly applies in the case of Facebook. The service they are promising
you for free, is not the service they intend to provide to you, but a
means to directing you to the services provided for their real customers,
the advertisers.

Regardless of your expectations, or their promises, or the apparent
utility of the services they offer, you cannot expect such 'advertiser-
paid' services to respect your privacy in any of the ways discussed
in this article.

Should we fear the State?
-------------------------

It is no surprise that surveillance of private citizens and suppression
of the media go hand-in-hand with despotism and authoritarianism.
Communist East Germany during the Cold War being the architypal example.

But Western democracies are starting to head in the same direction.
The US Government's surveillance program, PRISM, as revealed by
Edward Snowden, is shockingly comprehensive in its data collection
capabilities [^8].

The UK Government has developed perhaps the most comprehensive system
of video surveillance ever attempted and is combining it with facial
recognition to achieve an unpresidented ability to track individuals
whenever they venture into public spaces [^13].

While Western democracies defend their increasing use of mass surveillance
by stressing the threats of terrorism and the benefits of efficiency
in law enforcement generally, the implications are worrying.

The steady rise in nationalism and populism in the US and Europe hints
at the risk - Donald Trump has already shown a complete disdain for 
freedom of the press and the rights of minorities. What would prevent
such a leader from using mass surveillance to start a systematic
program of persecution against the free press, political critics and
the judiciary?

For the rest of us, simply meeting with a "person of interest",
attending a rally, or entering an STD clinic could mark you out for
special consideration. It is just as likely that you will be 
mis-classified. Even if you discover the mistake, what ability would
you have to correct it in the face of a resistant state bureaucracy?

In the movie Minority Report, crimes can be anticipated and criminals
convicted *before* they commit the crime. Increasingly mass
surveillance will encourage governments to intervene before a crime is
actually committed. The default of such a process is inevitably a
shift from presumtion of innocence to presumption of guilt.

Mass surveillance will also mark a shift in the treatment of minor
civil infractions. Laws for jay-walking, littering
and possession of small quantities of recreational drugs
operate on a sprit of judicial discretion, which in the past has
tended towards generosity. The 'spirit of the law' in these cases is
to offer a way to curb against widespread anti-social behaviour.
When such behaviour becomes commonplace, they can be enforced more
strongly, when someone has a pattern of general anti-social behaviour
they offer a way to specify the offence. Isolated incidents by
generally well-behaved citizens are rarely prosecuted. In a
surveillance-dominated world, the default would be to punish every
minor infraction automatically. Just as a teenager rebels against
parental house rules better suited to toddlers, the effect would
be to create widespread antipathy towards law enforcement and the
state in general [^14].

Our relative obscurity is an important protection against
direct interference by governments in our lives. But the continual
creep of government powers over the individual that are accompanying
mass surveillance makes us increasing vulnerable to overreach and abuse. 

The price we end up paying for a minimal enhancement in 'security',
is a major reduction in our freedoms an independent citizens,
and an increasing divergence between the interests of the government
and its citizens.

How secure is Government data?
------------------------------

The data government collects on us may not be nearly as secure as we
might hope. In several countries, medical records and social service
information have been accidentally leaked. Foreign agencies are
believed to target Government services' data repositories. [^15]

Governments have access to data we really don't expect to be made public.
We are willing to share detailed information about our finances with
taxation authorities, so that we can contribute fairly to the costs
of government, but would baulk if it were available to our neighbours, 
to our fellow workers, or to our friends.

Similarly out legal history. 
Do we want our employer to know about a parking fine,
a shoplifting caution when we were 16, or that night we got drunk in
Amsterdam and spent the night sobering up in a police cell?

Nor would we want our medical records made public. What would a
potential employer do if they knew we had an elevated genetic risk of 
stress-related heart disease? If insurance companies
knew more about your risk of early death than you do, based on 
their analysis of your parent's genes, would you be able to negotiate 
a fair rate with them?
How would you feel about a prospective partner knowing that you had
once taken a test for gonorrhoea?

The reality is that we must share some sensitive data with our
governments, but we expect that it will be kept confidential. Governments
have not shown they can take care of the data we already provide them
with, the greater the scope of the information they have on us, the
greater the risk.

What is the future of privacy and freedom online?
-------------------------------------------------

> Yes, there are two paths you can go by, but in the long run,
> there's still time to change the road you're on.

The path we are heading down is to accept that 'privacy', in all the
senses described above, is no longer a meaningful concept or a realistic
expectation as information about us becomes increasingly available to
the government, to corporations, to hackers, to employers and to
acquaintances. On this path, we submit to a fundamental change in our
'social contract'.

The Australian government is attempting to pass laws that will allow
it a 'back door' into all forms of online and phone communication and
access any data collected by corporations on its citizens. PRISM
demonstrated the extent to which this is already the case in the US. [^16]

In such a world, we must assume that everything about us
is knowable by everyone. We mustmanage our online 'brand' vigilantly.
This means submitting to a stereotyped version of ourselves, so we
neatly fit into the categories defined by marketers, governments and
cultural expectations, sublimating other facets of our personality. 
Those who fail to do so will become second-class citizens.
Once categorised, we must live with the consequences with little
opportunity to grow or change.

We must assume that everything we do is visible, and our behaviour
in public must be carefully orchestrated to prevent potential
mis-understandings of our intentions.

In short, our concept of what constitutes 'freedom' in a 'free'
society will be grealy curtailed.

What is the alternative?
------------------------

The other path is to take a stand before it is too late and actively
resist government and corporate actions that will reduce our privacy
and freedom, and wrest back control of our personal data.

Politically, the priority is to stop laws that allow governments to
increase their mass surveillance capabilities and press for laws that
put clear constraints on what is acceptable.

This means recognising that the use of phrases such as "nothing to hide",
"tough on crime", "efficient law enforcement", "necessary access" and 
"crime or terrorism prevention" are used to generate a knee-jerk reaction
of fear that will cause us to miscalculate the cost-benefit trade-off
of 'security' for 'privacy' and provide a cover for greater powers over
the individual.

It means making noise, voting, petitioning, marching and educating. 
It means communicating our concerns and initating public debate. 
The greatest danger is that too many people don't sufficiently 
understand the risks to care. Let's change that and ensure that
political parties face real consequences for their failure to protect
our privacy.

Commercially, we need to speak with our wallets. We must be willing to
leave services that fail to respect our needs and expectations of them
and move to alternatives with less incentive to abuse our trust.

Facebook is the most obviously egregious offender, and there is no
excuse for people who care about privacy to continue to use their
services. 
Apple is one company where the user, not the advertiser pays. As a
result its incentives are in your favour: Apple prominently promotes
itself as resisting government efforts to access customer data
and should be supported for this stand.

Even so, it is hard to know how far Apple's commitment to data privacy
extends and they appear to have largely capitulated to the demands of
the Chinese government to gain greater access to Chinese consumers.
As a general rule, all commercial providers should be viewed with
caution. Spread your data over multiple channels and providers,
set privacy settings to their maximum, put as little information as
possible in their sites and delete it when it is no longer relevant.
Read privacy statements and opt out where you can [^17].

In particular, take care with information that governments are likely
to value (your location, communcation and purchase details for example).
There is no reason to give up your fingerprints or facial features,
even it the alternative is a couple of seconds extra to secure your phone.

Secure what you can with good passwords. Independent password managers
such as Keeper, Dashlane and 1Password are invaluable tools for
managing strong, unique passwords across all your online accounts.

Use alternatives that are not advertiser supported, even if it means
paying a fee. A better model is 'freemium' in which small users get
access to a free service, that is largely paid for by business
customers. Skype and Dropbox are good examples of such services.

Technolgically, there is increasing involvement by the Open Source
community to build free, secure alternatives to commercial services, 
although high hosting costs and limited penetration will mean they
are likely to remain small.

There is a growing call, particularly amongst the advocates of the
early internet, to create decentralised services. In this model, there
is no central host that needs to be paid for, rather everyone on the
service contributes a small amount of storage and computing power
on behalf of the whole system. Since data is shared amongst all users,
there is no central repository that can be hacked or targeted by
governments, making many abuses of privacy almost impossible.

Whether the formidable technical hurdles, or education required to
roll out such a service can be overcome remains to be seen. [^18]

What can you do?
----------------

I for one am not ready to be led bleating down the first path toward
a dystopian future of totalitarian government surveillance, 
commercial subservience and vigilante social justice.

If you care, start with yourself: think hard about your interaction with
online services and start changing settings and deleting data.

Next, get out there and spread the word. Share this article, and do some
research of your own. Communicate, educate, protest, vote!

If you have software development skills, 
get involved in the Open Source community,
contribute to projects and help make a decentralised web a reality.

Footnotes [^19]

[^1]: This happened in 2009, see [this article](https://www.theguardian.com/technology/2009/dec/10/facebook-privacy).

[^2]: Although this occurred at the same time as #metoo, it's intentions were clearly unrelated, see [this article](https://www.theguardian.com/film/2018/jul/20/guardians-of-the-galaxy-james-gunn-fired).

[^3]: That is, confusingly for non-British people, an old, posh, elite private boarding school. Although mine was very much at the lower end of the scale, the same attitudes prevailed.

[^4]: Amazon.com links: [Marx](https://www.amazon.com/Communist-Manifesto-Chiron-Academic-Press/dp/9176374882/), [Koblitz](https://www.amazon.com/Course-Number-Cryptography-Graduate-Mathematics/dp/0387942939), [Schneier](https://www.amazon.com/Applied-Cryptography-Protocols-Algorithms-Source/dp/1119096723), [Levy](https://www.amazon.com/Hackers-Heroes-Computer-Revolution-Anniversary-ebook/dp/B003PDMKIY).

[^5]: Wikipedia links: [Anonymous](https://en.wikipedia.org/wiki/Anonymous_(group)), [TOR](https://en.wikipedia.org/wiki/Tor_%28anonymity_network%29), [4chan](https://en.wikipedia.org/wiki/4chan).

[^6]: Out of our discussions we formulated a [book list](https://gitlab.com/QuesterZen/YoungScientistBooks/blob/master/YoungScientistBooks.md) for his son / my (secular) godson.

[^7]: Cicada 3301 puzzle, see [this article](https://www.theguardian.com/technology/2014/jan/10/cicada-3301-i-tried-the-hardest-puzzle-on-the-internet-and-failed-spectacularly).

[^8]: The Guardian was the first newspaper to break the story with a series of articles, for example [here](https://www.theguardian.com/world/2013/jun/06/us-tech-giants-nsa-data), quickly followed by the Washington Post, for example [here](https://www.washingtonpost.com/investigations/us-intelligence-mining-data-from-nine-us-internet-companies-in-broad-secret-program/2013/06/06/3a0c0da8-cebf-11e2-8845-d970ccb04497_story.html).

[^9]: An example of the wealth of data available to Google and Facebook appears in [this article](https://www.theguardian.com/commentisfree/2018/mar/28/all-the-data-facebook-google-has-on-you-privacy).

[^10]: It is not clear what impact the Cambridge Analytica data had on the outcome of the 2016 election, but the most shocking aspect was that the majority of the data obtained concerned people who had not opted into the 'app' from which the data was gathered, as discussed for example [here](https://www.theguardian.com/news/2018/mar/17/cambridge-analytica-facebook-influence-us-election).

[^11]: As discussed in [this article](https://www.theguardian.com/uk-news/2017/aug/06/mary-beard-twitter-abuse-roman-britain-ethnic-diversity).

[^12]: Of course the Terms and Conditions we automatically click to "agree" to are far too long and dense to read, are in any case absurdly onerous and legalistic, and probably not enforceable, as discussed for example in [this article](https://www.theguardian.com/technology/2015/jun/15/i-read-all-the-small-print-on-the-internet).

[^13]: In 2016 the law in the UK changed giving the government increased surveillance powers as discussed in [this article](https://www.theguardian.com/world/2016/nov/19/extreme-surveillance-becomes-uk-law-with-barely-a-whimper). For an older discussion with statistics on UK CCTV cameras, see [this article](https://www.theguardian.com/world/2015/jan/06/tony-porter-surveillance-commissioner-risk-cctv-public-transparent).

[^14]: The 'three strikes' rule applied in several states in the US is a good example of the effect of taking discretionary sentencing out of the hands of judges. While effective in reducing minor crime, the laws have led to a great increase in the prison population (in the US, already far higher than in any other rich democracy), a disproportionate emprisoning of young Black males, and has increased tensions between the law enforcement and the community as both sides are aware that any minor offence could result in punitive punishment - assaults against police offers seem to have risen where these laws are applied. There is some evidence that it may also have led to an increase in some forms of major crime. Bill Clinton felt that such laws introduced during his presidency had ['made matters worse'](https://www.bbc.com/news/world-us-canada-33545971).

[^15]: In the US, the most severe example was from [The Department of Social Services](https://www.theguardian.com/technology/2017/nov/24/data-breach-hits-department-of-social-services-credit-card-system). In Australia, Medicare records were [hacked](https://www.theguardian.com/australia-news/2017/jul/04/the-medicare-machine-patient-details-of-any-australian-for-sale-on-darknet). In the UK the NHS has leaked or lost patient records on several occasions, including [this report](https://securitybrief.eu/story/breaking-nhs-accidentally-leaks-data-150000-patients/). Direct hacking attacks by foreign agencies are less well reported, but probably include the US's [Office of Personnel Management](https://www.washingtonpost.com/news/federal-eye/wp/2015/07/09/hack-of-security-clearance-system-affected-21-5-million-people-federal-authorities-say).

[^16]: This law, or something like it, is looking increasingly likely to be passed. It is explained in [this article](https://www.theguardian.com/technology/2018/jun/06/planned-laws-to-force-tech-firms-to-reveal-encrypted-data) and some of the more concerning implications are referred to in [this editorial](https://www.zdnet.com/article/australias-anti-encryption-law-will-merely-relocate-the-backdoors-expert/). PRISM revealed the extent to which the us Congress has supported the NSA's ability to gain access to commercial data, as described [here](https://www.washingtonpost.com/opinions/nsa-surveillance-may-be-legal--but-its-unconstitutional/2013/06/21/b9ddec20-d44d-11e2-a73e-826d299ff459_story.html?utm_term=.b6444f88d069). The NSA and FBI are believed to have the technological capability to defeat several commercial [encryption schemes](https://www.theguardian.com/world/2013/sep/05/nsa-gchq-encryption-codes-security) including the security on the [iPhone](https://www.theguardian.com/technology/2016/mar/09/edward-snowden-fbi-san-bernardino-iphone-bullshit-nsa-apple).

[^17]: The European General Data Protection Regulation is some help as company's are forced to explicitly let you know what data they have on you and allow you to remove some data. However, while it offers legal protections to consumers, there is little evidence that consumers will do much more than accept the nagging requests made by service providers to 'opt in' on all requests, since the alternative would be continual nagging or restricted services. A summary of GDPR is available [here](https://www.theguardian.com/technology/2018/may/21/what-is-gdpr-and-how-will-it-affect-you).

[^18]: TOR and BitTorrent are examples of this type of decentralised data storage system. Another discussion of the decentralised web can be found in [this article](https://www.theguardian.com/technology/2018/sep/08/decentralisation-next-big-step-for-the-world-wide-web-dweb-data-internet-censorship-brewster-kahle).

[^19]: I have mainly used The Guardian and Washington Post as sources for news articles. There are two reasons for this:
Firstly, these publications have been particularly strong in their defence and exposure of data privacy issues; secondly, other publications with good articles such as The New York Times, The Economist and The Atlantic place limits on access to their online content. If you are interested in a particular topic, I strongly encourage you to perform your own search. Wikipedia Articles' References sections are a often a good place to start.
